import os
import sys
import time

import boto3
from botocore.exceptions import ClientError
from bitbucket_pipes_toolkit import get_logger, fail, success

MAX_COUNTER = 30

logger = get_logger()


def delete_aws_stack(stack_name):
    try:
        aws_client_cf = boto3.client('cloudformation', region_name=os.getenv('AWS_DEFAULT_REGION'))
    except ClientError as err:
        fail("Failed to create boto3 client.\n" + str(err))

    stacks = aws_client_cf.describe_stacks(StackName=stack_name)
    logger.info(stacks)
    stack_status = stacks['Stacks'][0]['StackStatus']

    # Initial checks to not request stack deletion if stack is in DELETE statuses.
    if stack_status == 'DELETE_IN_PROGRESS':
        fail('Stack is still deleting. Re-run this step later.')
    elif stack_status == 'DELETE_COMPLETE':
        success('Stack successfully deleted.')
    elif stack_status == 'DELETE_FAILED':
        # Delete stack with retain of failed resources after deletion failed.
        aws_client_cf.delete_stack(
            StackName=stack_name,
            RetainResources=['sampleConfigurationTemplate']
        )
        success('Initiated deletion on AWS with retain resources.')
    # Case when stack should be requested for deletion: stack is not in DELETE statuses.
    # This case should run only 1 time, with next runs stack should be in DELETE statuses.
    else:
        # Delete stack
        aws_client_cf.delete_stack(
            StackName=stack_name
        )

        stacks = aws_client_cf.describe_stacks(StackName=stack_name)
        stack_status = stacks['Stacks'][0]['StackStatus']
        logger.info(f'Stack status: {stack_status}')

        attempt = 0
        while stack_status == 'DELETE_IN_PROGRESS' and attempt < MAX_COUNTER:
            stacks = aws_client_cf.describe_stacks(StackName=stack_name)
            stack_status = stacks['Stacks'][0]['StackStatus']
            logger.info(f'Stack status: {stack_status}')

            time.sleep(10)
            attempt += 1

        if stack_status == 'DELETE_FAILED':
            # Delete stack with retain of failed resources after deletion failed.
            aws_client_cf.delete_stack(
                StackName=stack_name,
                RetainResources=['sampleConfigurationTemplate']
            )
            success('Initiated deletion on AWS with retain resources.')

        # Case when max wait time reached, but stack could not be deleted with retain resources.
        if stack_status == 'DELETE_IN_PROGRESS':
            fail('Stack is still deleting. Re-run this step.')


if __name__ == '__main__':
    globals()[sys.argv[1]](sys.argv[2])
